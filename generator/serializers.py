from rest_framework import serializers

from generator.models import CertTemplate


class CertTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CertTemplate
        fields = ('name', 'version', 'fields')
