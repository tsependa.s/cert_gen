from django.contrib import admin

# Register your models here.
from generator.models import CertTemplate


class TemplateAdmin(admin.ModelAdmin):
    pass

admin.site.register(CertTemplate)