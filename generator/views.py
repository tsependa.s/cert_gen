# -*- coding: utf-8 -*-
import urllib
from subprocess import call

import imgkit
import pdfkit
from PIL import Image, ImageChops
from django.http import JsonResponse, HttpResponse, Http404, response
from django.template import TemplateDoesNotExist
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets

from generator.models import CertTemplate
from generator.serializers import CertTemplateSerializer


@csrf_exempt
def cert_template_list(request):
    if request.method == 'GET':
        templates = CertTemplate.objects.all()
        serializer = CertTemplateSerializer(templates, many=True)
        return JsonResponse(serializer.data, safe=False)


class TemplateViewSet(viewsets.ModelViewSet):
    queryset = CertTemplate.objects.all()
    serializer_class = CertTemplateSerializer

@csrf_exempt
def generate_cert_jpg(request, name):
    if request.method == 'GET':

        cert_template = CertTemplate.objects.get(name=name)
        urltemplate = request.get_host() + "/cert_templates/" + cert_template.name + "/" + str(
            cert_template.version) + "/template.html?"

        params = request.GET
        for key in params:
            urltemplate = urltemplate + key + "=" + params[key] + "&"

        urltemplate = "http://" + urllib.parse.quote(urltemplate, safe=':/?&=')
        call(["phantomjs", "rasterize.js", urltemplate, "out.jpg"])

        response = HttpResponse(content_type='image/jpg;')
        response['Content-Disposition'] = 'inline;filename=out.jpg'
        response['Content-Transfer-Encoding'] = 'binary'
        with open('out.jpg', 'rb') as png:
            response.write(png.read())
        return response



@csrf_exempt
def generate_cert_png(request, name):
    if request.method == 'GET':

        cert_template = CertTemplate.objects.get(name=name)
        urltemplate = request.get_host() + "/cert_templates/" + cert_template.name + "/" + str(
            cert_template.version) + "/template.html?"

        params = request.GET
        for key in params:
            urltemplate = urltemplate + key + "=" + params[key] + "&"

        urltemplate = "http://" + urllib.parse.quote(urltemplate, safe=':/?&=')
        call(["phantomjs", "rasterize.js", urltemplate, "out.png"])

        response = HttpResponse(content_type='image/png;')
        response['Content-Disposition'] = 'inline;filename=out.png'
        response['Content-Transfer-Encoding'] = 'binary'
        with open('out.png', 'rb') as png:
            response.write(png.read())
        return response


        #print(urltemplate)
        #imgkit.from_url(urltemplate, 'out.png')

        #response = HttpResponse(content_type="image/png")
        #img = Image.open('out.png')
        # bg = Image.new(img.mode, img.size, img.getpixel((0, 0)))
        # diff = ImageChops.difference(img, bg)
        # bbox = diff.getbbox()
        # if bbox:
        #    img = img.crop(bbox)
        #img.save(response, 'png')
        #return response


def generate_cert_pdf(request, name):
    if request.method == 'GET':

        cert_template = CertTemplate.objects.get(name=name)
        urltemplate = request.get_host() + "/cert_templates/" + cert_template.name + "/" + str(
            cert_template.version) + "/template.html?"

        params = request.GET
        for key in params:
            urltemplate = urltemplate + key + "=" + params[key] + "&"


        urltemplate = "http://" + urllib.parse.quote(urltemplate, safe=':/?&=')
        call(["phantomjs", "rasterize.js", urltemplate, "out.pdf"])

        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = 'inline;filename=out.pdf'
        response['Content-Transfer-Encoding'] = 'binary'
        with open('out.pdf', 'rb') as pdf:
            response.write(pdf.read())
        return response


class StaticView(TemplateView):
    def get(self, request, page, *args, **kwargs):

        self.template_name = page
        response = super(StaticView, self).get(request, *args, **kwargs)

        try:
            return response.render()
        except TemplateDoesNotExist:
            raise Http404()

    def get_context_data(self, **kwargs):
        context = super(StaticView, self).get_context_data(**kwargs)
        params = self.request.GET
        for key in params:
            context[key] = params[key]
        return context
