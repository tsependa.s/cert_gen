import re
import zipfile

from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver


class CertTemplate(models.Model):
    name = models.CharField(max_length=255)
    version = models.IntegerField()
    template_zip = models.FileField(upload_to="template_zip")
    fields = models.CharField(max_length=1000)


@receiver(post_save, sender=CertTemplate)
def unzip_and_process(sender, instance, **kwargs):
    file_path = instance.template_zip.path
    with zipfile.ZipFile(file_path, 'r') as template_zip:
        template_zip.extractall(path="cert_templates"+"/"+instance.name+"/"+str(instance.version))
        template_zip.close()