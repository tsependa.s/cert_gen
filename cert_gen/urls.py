"""cert_gen URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles import views as staticviews

from django.urls import path, re_path, include
from rest_framework import routers

import generator
from generator import views
from generator.views import StaticView, TemplateViewSet

router = routers.DefaultRouter()
router.register(r'templates', TemplateViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^templates/$', views.cert_template_list),
    url(r'gencertpng/(?P<name>\w+)$', views.generate_cert_png),
    url(r'gencertjpg/(?P<name>\w+)$', views.generate_cert_jpg),
    url(r'gencertpdf/(?P<name>\w+)$', views.generate_cert_pdf),
    url(r'^cert_templates/(?P<page>.+\.html)$', StaticView.as_view()),
    re_path(r'^cert_templates/(?P<path>.*)$', staticviews.serve),
]

